# -*- coding: utf-8 -*-
#
# Copyright (C) 2013 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Command-line skeleton application for Translate API.
Usage:
  $ python sample.py

You can also get help on all the command-line flags the program understands
by running:

  $ python sample.py --help

"""

import httplib2
import sys
import argparse
import codecs
import re
import operator
import string
import nltk
import nltk.tokenize

from collections import defaultdict
from apiclient import discovery


ignoreList = ['-', 'Stephen', 'Harvey', 'James', 'Jean-Pierre', 'Metcalfe', 'Robin', 'Harveya']

def main(argv):
    # Create an httplib2.Http object to handle our HTTP requests .
    http = httplib2.Http()

    # Construct the service object for the interacting with the Translate API.
    #service = discovery.build('translate', 'v2',
            #developerKey='AIzaSyCyFUR5EAE49M8eJS-sLAjHjjy7YhERyYA', http=http)

    parser = argparse.ArgumentParser(description='Create A CSV file of all the\
            words translated from Google Translate')
    parser.add_argument('-i', '--input', dest='input',
            required=True,
            help='Input file to be parsed')
    parser.add_argument('-o', '--output', dest='output',
            required=True,
            help='Output CSV filename to be generated')
    s = vars(parser.parse_args())
    inputFile = codecs.open(s['input'], encoding='utf-8-sig', mode="r")
    inputLines = inputFile.read()
    wordList = nltk.regexp_tokenize(inputLines, r'[ -,;_\d\n\r\.\?!"]\s*', gaps=True)
    wordList = [word for word in wordList if word not in ignoreList]
    wordFrequency = defaultdict(int)
    for word in wordList:
        wordFrequency[word] += 1

    sortedWordFrequency = sorted(wordFrequency.iteritems(), key=operator.itemgetter(1), reverse=True)

    outputFile = codecs.open(s['output'], encoding='utf-8', mode='w')
    for k,v in sortedWordFrequency:
        outputFile.write(u"{} - {}\n".format(v, k))
    outputFile.close()

    '''
    print service.translations().list(
      source='pl',
      target='en',
      q=['flower', 'car']
    ).execute()
    '''

# For more information on the Translate API you can visit:
#
#   https://developers.google.com/translate/v2/using_rest
#
# For more information on the Translate API Python library surface you
# can visit:
#
#   https://developers.google.com/resources/api-libraries/documentation/translate/v2/python/latest/
#
# For information on the Python Client Library visit:
#
#   https://developers.google.com/api-client-library/python/start/get_started
if __name__ == '__main__':
  main(sys.argv)
