proseStore = "https://lltf.firebaseio.com/prose/en"
translationStore = "https://lltf.firebaseio.com/translations/"
service_url = "https://www.googleapis.com/language/translate/v2"
translation_data = {
    key : 'AIzaSyCyFUR5EAE49M8eJS-sLAjHjjy7YhERyYA ',
    source : 'pl'
    target: 'en'
}

escapeRegExp = (s) ->
    String(s).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1')

generateClassName = (name) ->
    name.replace(/[^a-z0-9]/g, (s) ->
        c = s.charCodeAt(0)
        if (c == 32)
            '-'
        else if (c >= 65 && c <= 90)
            '_' + s.toLowerCase()
        else
            '__' + ('000' + c.toString(16)).slice(-4)
    )

getSelectionText = (el) ->
    selObj = window.getSelection()
    text = selObj.toString()
    if !validate(text)
        return false
    selRange = selObj.getRangeAt(0)

    newElement = document.createElement("abbr")
    newElement.className = "translated-word " + generateClassName(text)
    #selRange.surroundContents(newElement)

    selObj.removeAllRanges()
    
    findAndReplaceDOMText(el, {
        find: new XRegExp("\\P{L}" + text + "\\P{L}", 'gi')
        wrap: newElement
    })
    text

validate = (str) ->
  $.trim(str).length != 0

qtip_defaults = {
        content: {
            text: (event, api) ->
                $(this).attr('data-translation')
        }
        style: {
           classes: 'qtip-dark'
        }
        position: {
          my: 'bottom center'
          at: 'top center'
          viewport: true
        }
        show: {
          ready: true
          solo: true
        }
        hide: {
          target: $(document)
          event: 'click'
        }
      }

qtip_autoload_defaults = qtip_defaults
qtip_autoload_defaults.show.ready = false

loadProse = (el) ->
    deferred = Q.defer()
    proseObj = new Firebase(proseStore).limit(20)
    proseObj.on("child_added", (snapshot) ->
        if snapshot.val()
            $(el).html($(el).html() + " " + snapshot.val().prose)
            deferred.resolve(snapshot.val().prose)
        else
            error = new Error("No Prose Found")
            $(el).text("No Text found")
            deferred.reject(error)
    )
    deferred.promise

saveProse = () ->
    if Modernizr.localstorage
        $(".prose[data-autoload]").each( () ->
            localStorage.prose = $(this).html()
        )

$ ->
  $(".prose[data-autoload]").each( () ->
      el = this
      if $(el).html().length == 0
        if Modernizr.localstorage
            if localStorage.prose
                $(el).html(localStorage.prose)
            else
                loadProse(el).then((data) ->
                    localStorage.prose = data
                )
        else
            loadProse(el)
  )

  $("[data-translation]").qtip(qtip_autoload_defaults)

  $("[data-translate]").dblclick (e) ->
    if $(e.target).hasClass("translated-word")
        return

    sourceText = getSelectionText(this)
    if validate(sourceText)
        translatedObj = new Firebase(translationStore + "/" + translation_data.source + "/" + translation_data.target + "/" + sourceText)
        translatedObj.on("value", (snapshot) ->
            if snapshot.val()
                content = snapshot.val()
                qtip_defaults.style.classes = 'qtip-dark'
                $("." + generateClassName(sourceText)).each(->
                    this.setAttribute('data-translation', content)
                    $(this).qtip(qtip_defaults)
                )
                saveProse()
            else
                $.extend(translation_data, {q: sourceText})
                $.ajax({
                    type: "GET"
                    dataType: "jsonp"
                    data: translation_data
                    url: service_url
                }).then((data) ->
                    content = data.data.translations[0].translatedText
                    translatedObj = new Firebase(translationStore + "/" + translation_data.source + "/" + translation_data.target)
                    translatedObj.child(sourceText).set(content)
                    qtip_defaults.style.classes = 'qtip-light'
                    $("." + generateClassName(sourceText)).each(->
                        this.setAttribute('data-translation', content)
                        $(this).qtip(qtip_defaults)
                    )
                    saveProse()
                , (xhr, status, error)->
                    qtip_defaults.style.classes = 'qtip-red'
                    #TODO: Comeback to this and set error status somewhere
                    #sourceText.setAttribute('data-translation', status + ': ' + error)
                    #$(sourceText).qtip(qtip_defaults)
                    console.warn status, error
                )
        )
